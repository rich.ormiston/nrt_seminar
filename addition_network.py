import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import numpy as np
np.random.seed(1337)
from keras.models import Sequential
from keras.layers import Dense

# make a list of 100 pairs from 0-25 for the network to add together
input_data = np.random.randint(25, size=(100,2))
target_data = np.array([x[0] + x[1] for x in input_data])
for ii in range(10):
    a, b = input_data[ii, :]
    ans = target_data[ii]
    print('{0:>2} + {1:>2} = {2:>2}'.format(a, b, ans))

# Now that we have our data set, let's split it into training and testing data
train_size = 75

# this is the data we train the network on
x_train = input_data[:train_size, :]
y_train = target_data[:train_size]

# this is the data that we test to see how well we did during training
x_test = input_data[train_size:, :]
y_test = target_data[train_size:]

print('x_train.shape={0}, y_train.shape={1}'.format(x_train.shape, y_train.shape))
print('x_test.shape={0}, y_test.shape={1}'.format(x_test.shape, y_test.shape))

print('Running the network...')
model = Sequential()
model.add(Dense(2, input_dim=x_train.shape[1], activation='linear'))
model.add(Dense(1, activation='linear'))
model.compile(loss='mse', optimizer='adam')
history = model.fit(x_train, y_train, epochs=300, validation_data=(x_test, y_test), batch_size=1, verbose=0)
y_hat = model.predict(x_test)

# let's log the loss functions and plot them
train_loss = history.history['loss']
val_loss = history.history['val_loss']

plt.plot(train_loss, label='Training Loss')
plt.plot(val_loss, label='Validation Loss')
plt.legend()
plt.title('Loss History')
plt.xlabel('Epochs')
plt.ylabel('Loss (MSE)')
plt.savefig('addition_loss.png')
plt.close()

# print out the network estimates
for ii in range(x_test.shape[0]):
    a, b = x_test[ii, :]
    true_ans = y_test[ii]
    network_ans = y_hat[ii][0]
    print('{0:>2} + {1:>2} = {2:<10}'.format(a,b,network_ans))

weights0, biases0 = model.layers[0].get_weights()
weights1, biases1 = model.layers[1].get_weights()

print(weights0.T, biases0, '\n')
print(weights1, biases1)

tot_weights = weights1.T.dot(weights0.T)
print(tot_weights)
