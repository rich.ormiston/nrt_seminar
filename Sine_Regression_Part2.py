# do imports
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import numpy as np
np.random.seed(3301)
import scipy.signal as sig
from keras.models import Sequential
from keras.layers import Dense
import scipy.signal as sig


# let's define the signals
dur = 512
sample_rate = 128
true_freq = 13.4
noise_freq = 5.8
noise_phase = np.pi/2
noise_amp = 0.25
t = np.linspace(0, dur, dur*sample_rate)
s = np.sin(2*np.pi*true_freq * t)
n = noise_amp * np.sin(2*np.pi*noise_freq*t + noise_phase)
d = s + noise_amp * np.sin(2*np.pi*noise_freq*t)  # phase shifted input

# let us again make the training and testing data,
# but this time we wil split the data into 1 second chunks
input_data = np.zeros((dur, sample_rate))
target_data = np.zeros((dur, sample_rate))
for ii in range(dur):
    st = ii*sample_rate
    et = (ii+1)*sample_rate
    input_data[ii, :] = n[st:et]
    target_data[ii, :] = d[st:et]

print('input data shape:', input_data.shape)
print('target data shape:', target_data.shape)

train_dur = 504  # seconds
x_train = input_data[:train_dur, :]
y_train = target_data[:train_dur, :]

x_test = input_data[train_dur:, :]
y_test = target_data[train_dur:, :]

print('x_train, y_train', x_train.shape, y_train.shape)
print('x_test, y_test', x_test.shape, y_test.shape)

model = Sequential()  # so far so good
model.add(Dense(2*sample_rate, input_dim=x_train.shape[1], activation='linear'))
model.add(Dense(sample_rate))
model.compile(loss='mse', optimizer='adam')
history = model.fit(x_train, y_train, epochs=50, validation_data=(x_test, y_test), batch_size=sample_rate//2)
y_hat = model.predict(x_test)

 # let's log the loss functions and plot them
train_loss = history.history['loss']
val_loss = history.history['val_loss']
plt.plot(train_loss, label='Training Loss')
plt.plot(val_loss, label='Validation Loss')
plt.axhline(0.5, ls='--', color='black')
plt.legend()
plt.title('Loss History')
plt.xlabel('Epochs')
plt.ylabel('Loss (MSE)')
plt.savefig('sine_regression_part2_loss.png')
plt.close()

# get the cleaned estimate and plot it
clean = y_test.flatten() - y_hat.flatten()
mse = ((clean - s[-sample_rate*8:])**2).mean()

time = np.linspace(0, 1, sample_rate)
plt.plot(time, y_test.flatten()[-sample_rate:], label='System Signal', lw=3.0)
plt.plot(time, s[-sample_rate:], label='True Signal', lw=2.0)
plt.plot(time, clean[-sample_rate:], label='Cleaned Estimate', ls='--')
plt.legend(loc='upper right')
plt.grid(True)
plt.xlabel('Time [s]')
plt.title('Estimate vs True Signal')
plt.savefig('sine_regression_part2_estimate.png')
plt.close()

noise_floor = 0.01 * np.random.rand(sample_rate*8)
freq, clean_psd = sig.welch(clean+noise_floor, fs=sample_rate, nperseg=sample_rate*8)
_, signal_psd = sig.welch(s[-sample_rate*8:]+noise_floor, fs=sample_rate, nperseg=sample_rate*8)
_, d_psd = sig.welch(d[-sample_rate*8:]+noise_floor, fs=sample_rate, nperseg=sample_rate*8)
plt.semilogy(freq, d_psd, label='System Signal')
plt.semilogy(freq, signal_psd, label='True Signal')
plt.semilogy(freq, clean_psd, label='True Signal Estimate')
plt.axvline(noise_freq, label='$f_n={}$ Hz'.format(noise_freq), ls='--', color='black', alpha=0.5)
plt.axvline(true_freq, label='$f_s={}$ Hz'.format(true_freq), ls='--', color='black', alpha=0.5)
plt.legend(loc='upper right')
plt.xlabel('Frequency [Hz]')
plt.ylabel('Power Spectral Density')
plt.xlim([3,30])
plt.ylim([1e-9, 1e2])
plt.grid(True)
plt.title('PSD of Target and Network Output')
plt.savefig('sine_regression_part2_psd.png')
plt.close()
