# do imports
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import numpy as np
np.random.seed(3301)
import scipy.signal as sig
import scipy.io as sio
from keras.models import Sequential
from keras.layers import Dense, LSTM

# channel names
darm_chan = 'H1:GDS-CALIB_STRAIN'
pem_chan  = 'H1:PEM-CS_MAINSMON_EBAY_1_DQ'
asc_chans = ['H1:ASC-PRC1_Y_INMON',
             'H1:ASC-PRC2_P_INMON',
             'H1:ASC-SRC2_Y_INMON',
             'H1:ASC-DHARD_Y_INMON',
             'H1:ASC-INP1_P_INMON',
             'H1:ASC-INP1_Y_INMON',
             'H1:ASC-MICH_P_INMON',
             'H1:ASC-MICH_Y_INMON',
             'H1:ASC-PRC1_P_INMON',
             'H1:ASC-PRC2_Y_INMON',
             'H1:ASC-SRC1_P_INMON',
             'H1:ASC-SRC1_Y_INMON',
             'H1:ASC-SRC2_P_INMON',
             'H1:ASC-DHARD_P_INMON',
             'H1:ASC-CHARD_P_INMON',
             'H1:ASC-CHARD_Y_INMON',
             'H1:ASC-DSOFT_P_INMON',
             'H1:ASC-DSOFT_Y_INMON',
             'H1:ASC-CSOFT_P_INMON',
             'H1:ASC-CSOFT_Y_INMON']

# let global params
fs_fast, fs_slow, dur = 256, 16, 2048  # Sampling rates and segment duration
st = 1242457832  # GPS Start Time
et = st + dur
flow, fhigh = 56, 64  # frequency band of interest

def butter_filter(dataset,
                 low   = 4.0,
                 high  = 20.0,
                 order = 8,
                 btype = 'bandpass',
                 fs    = 512):
    """
    Phase preserving filter (bandpass, lowpass or highpass)
    based on a butterworth filter

    Parameters
    ----------
    dataset : `numpy.ndarray`
        input data (chans x lenght) where chan==0 is target
    lowcut : `float`
        low knee frequency
    highcut : `float`
        high knee frequency
    order : `int`
        filter order
    btype : `str`
        type of filter (bandpass, highpass, lowpass)
    fs : `int`
        sample rate

    Returns
    -------
    dataset : `numpy.ndarray`
        filtered dataset
    """

    # Normalize the frequencies
    nyq  = 0.5 * fs
    low  /= nyq
    high /= nyq

    # Make and apply filter
    if 'high' in btype:
        z, p, k = sig.butter(order, low, btype=btype, output='zpk')
    elif 'band' in btype:
        z, p, k = sig.butter(order, [low, high], btype=btype, output='zpk')
    elif 'low' in btype:
        z, p, k = sig.butter(order, high, btype=btype, output='zpk')
    sos = sig.zpk2sos(z, p, k)

    if dataset.ndim == 2:
        for i in range(dataset.shape[0]):
            dataset[i, :] = sig.sosfiltfilt(sos, dataset[i, :])
    else:
        dataset = sig.sosfiltfilt(sos, dataset)

    return dataset

# load the data and upsample the slow channels
darm = sio.loadmat('darm.mat')['data']
darm = darm.reshape(darm.shape[1])
darm = butter_filter(darm.flatten(), low=flow, high=fhigh, fs=fs_fast)

pem = sio.loadmat('pem.mat')['data']
pem = pem.reshape(pem.shape[1])

asc = sio.loadmat('asc.mat')['data']
asc = sig.resample(asc[1:5, :], dur*fs_fast, axis=1)  # let's just use 4 ASC channels instead of 20

# make some input data plots
freqs, darm_psd = sig.welch(darm, fs=fs_fast, nperseg=fs_fast*60)
_, mains_psd = sig.welch(pem, fs=fs_fast, nperseg=fs_fast*60)
_, asc1_psd = sig.welch(asc[0 ,:], fs=fs_fast, nperseg=fs_fast*60)
plt.semilogy(freqs, darm_psd.flatten(), label='DARM')
plt.semilogy(freqs, mains_psd.flatten(), label='Power Mains')
plt.semilogy(freqs, asc1_psd.flatten(), label='ASC Chan 0')
plt.legend(loc='lower right')
plt.grid(True)
plt.xlabel('Frequency [Hz]')
plt.ylabel('Power Spectral Density')
plt.title('Power Spectrum of DARM and Select Wits')
plt.savefig('sidebands_input_psds.png')
plt.close()

# do standardization and plot again
darm = (darm - np.mean(darm))/np.std(darm)
pem = (pem - np.mean(pem))/np.std(pem)
for ix in range(asc.shape[0]):
    asc[ix, :] = (asc[ix, :] - np.mean(asc[ix, :])) / np.std(asc[ix, :])

wits = np.vstack((pem, asc))  # combine witnesses into a single matrix

freqs, darm_psd = sig.welch(darm, fs=fs_fast, nperseg=fs_fast*60)
_, mains_psd = sig.welch(pem, fs=fs_fast, nperseg=fs_fast*60)
_, asc1_psd = sig.welch(asc[0 ,:], fs=fs_fast, nperseg=fs_fast*60)
plt.semilogy(freqs, darm_psd.flatten(), label='DARM')
plt.semilogy(freqs, mains_psd.flatten(), label='Power Mains')
plt.semilogy(freqs, asc1_psd.flatten(), label='ASC Chan 0')
plt.legend(loc='lower right')
plt.grid(True)
plt.xlabel('Frequency [Hz]')
plt.ylabel('Power Spectral Density')
plt.title('Standardized Power Spectrum of DARM and Select Wits')
plt.savefig('sideband_input_std_psds.png')
plt.close()

# split into training and testing data
test_dur = 60
tfrac = (2048-test_dur)*fs_fast
x_train = wits[:, :tfrac].T
y_train = darm[:tfrac]

x_test = wits[:, tfrac:].T
y_test = darm[tfrac:]

# Build and run the network
model = Sequential()
model.add(Dense(4, input_dim=wits.shape[0], activation='elu'))
model.add(Dense(4, activation='elu'))
model.add(Dense(1))
model.compile(loss='mse', optimizer='adam')
history = model.fit(x_train, y_train, epochs=20, validation_data=(x_test, y_test), batch_size=1000)

y_hat = model.predict(x_test).flatten()
clean = y_test - y_hat

# plot prediction PSD
nps = fs_fast*16
freq, tar = sig.welch(y_test, fs=fs_fast, nperseg=nps)
_, cln = sig.welch(clean, fs=fs_fast, nperseg=nps)
cln = cln.reshape(len(freq))
tar = tar.reshape(len(freq))

fig, ax = plt.subplots(2, 1, sharex=True, sharey=False, figsize=(6, 6))
ax[0].semilogy(freq, tar, label='DARM')
ax[0].semilogy(freq, cln, label='Clean')
ax[0].set_title('Network Results')
ax[0].set_ylabel('Scaled Strain')
ax[0].set_ylim([1e-5, 5e1])
ax[0].set_xlim([flow+1, fhigh-1])
ax[0].legend()
ax[0].grid(True)

ax[1].semilogy(freq, cln/tar, label='Bilinear Sub', color='red', lw=1.5)
ax[1].semilogy(freq, np.ones(tar.size), ls='--', color='black')
ax[1].set_ylabel('Scaled Strain')
ax[1].set_xlabel('Frequency [Hz]')
ax[1].set_ylim([1e-1, 2])
ax[1].legend()
ax[1].grid(True)

plt.tight_layout()
plt.savefig('sideband_output1_psd.png')
plt.close()

# plot prediction timeseries data
plt.plot(clean[-64:], label='Cleaned', ls='--')
plt.plot(y_test[-64:], label='Target')
plt.plot(y_hat[-64:], label='Prediction')
plt.legend()
plt.title('Time Series')
plt.xlabel('Time [s/$f_s$]')
plt.grid(True)
plt.savefig('sideband_output1_ts.png')
plt.close()


# define lookback
def do_lookback(data, steps=1, validation=False):
    """
    modify the dataset to include previous timesteps to feed into the
    recurrent network. this helps to capture long-term dependencies

    Parameters
    ----------
    data : `ndarray`
        numpy array containing the initial dataset
    steps : `int`
        number of previous time-steps to include
    validation : `bool`
        used for handling 1D arrays

    Returns
    -------
    temp : `ndarray`
        data array with added (previous) time-steps. if original array
        was M x N and L steps of lookback were requested, then the output
        is of shape M x L x N

    """
    temp = np.zeros((data.shape[0] - steps, steps + 1, data.shape[1]))
    temp[:, 0, :] = data[steps:, :]
    for i in range(temp.shape[0]):
        temp[i, 1:] = data[i:i + steps][::-1]

    if validation:
        temp = temp.reshape((temp.shape[0], temp.shape[1]))

    for i in range(temp.shape[0]):
        temp[i, :] = temp[i, :][::-1]

    return temp


# apply 8 steps of lookback (anything >= int(ceil(256/60)) should work)
wits = np.vstack((pem, asc))
lb = 8
darm_lb = darm[8:]
wits_lb = do_lookback(wits.T, 8)

# reshape for training and testing (samples, lookback, features)
tfrac = 60*fs_fast
x_train = wits_lb[:-tfrac, :, :]
y_train = darm_lb[:-tfrac]

x_test = wits_lb[-tfrac:, :, :]
y_test = darm_lb[-tfrac:]

# We can now run our first LSTM recurrent neural network!
model = Sequential()
model.add(LSTM(4, input_shape=(x_train.shape[1], x_train.shape[2]), activation='elu'))
model.add(Dense(1))
model.compile(loss='mse', optimizer='adam')
history = model.fit(x_train, y_train, epochs=20, validation_data=(x_test, y_test), batch_size=1000)

# make predictions and plots
y_hat = model.predict(x_test)
y_hat = y_hat.flatten()
clean = y_test - y_hat

nps = fs_fast*16
freq, tar = sig.welch(y_test, fs=fs_fast, nperseg=nps)
_, cln = sig.welch(clean, fs=fs_fast, nperseg=nps)
cln = cln.reshape(len(freq))
tar = tar.reshape(len(freq))

fig, ax = plt.subplots(2, 1, sharex=True, sharey=False, figsize=(6, 6))
ax[0].semilogy(freq, tar, label='DARM')
ax[0].semilogy(freq, cln, label='Clean')
ax[0].set_title('LSTM Network Results')
ax[0].set_ylabel('Scaled Strain')
ax[0].set_ylim([1e-6, 5e1])
ax[0].set_xlim([flow, fhigh])
ax[0].legend()
ax[0].grid(True)

ax[1].semilogy(freq, cln/tar, label='Clean/Target', color='red', lw=1.5)
ax[1].semilogy(freq, np.ones(tar.size), ls='--', color='black')
ax[1].set_ylabel('Scaled Strain')
ax[1].set_xlabel('Frequency [Hz]')
ax[1].set_ylim([np.min(cln/tar)/5, 2])
ax[1].legend()
ax[1].grid(True)

plt.tight_layout()
plt.savefig('sideband_output2_psd.png')
plt.close()
