# do imports
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import numpy as np
np.random.seed(3301)
from keras.models import Sequential
from keras.layers import Dense
import scipy.signal as sig


# let's define the signals
dur = 32
sample_rate = 128
true_freq = 13.4
noise_freq = 5.8
noise_phase = np.pi/4
noise_amp = 0.25
t = np.linspace(0, dur, dur*sample_rate)
s = np.sin(2*np.pi*true_freq * t)
n = noise_amp * np.sin(2*np.pi*noise_freq*t + noise_phase)

# when we generate the signal, we will adjust the phase so that the witness to
# the noise it out of phase. This will force the network to learn the phase as
# well as the amplitude. this is the last time that we will reference 's'
# until the end
d = s + noise_amp * np.sin(2*np.pi*noise_freq*t)

# plot the combined system output and the noise
vis = 2 * sample_rate
plt.plot(t[:vis], d[:vis], label='System Output Signal')
plt.plot(t[:vis], n[:vis], label='Noise')
plt.legend()
plt.xlabel('Time [s]')
plt.ylabel('Amplitude')
plt.savefig('sine_regression_part1_inputs.png')
plt.close()

# make training and testing data
# let's train on 24 seconds and test on 8
x_train = n[:24*sample_rate]
y_train = d[:24*sample_rate]

x_test = n[24*sample_rate:]
y_test = d[24*sample_rate:]

# build the network. We will output 1 data point for each input point. So this
# network is "one-to-one" or "single-in-single-out"
model = Sequential()
model.add(Dense(2, input_dim=1, activation='linear'))
model.add(Dense(1, activation='linear'))
model.compile(loss='mse', optimizer='adam')
history = model.fit(x_train, y_train, epochs=60,
    validation_data=(x_test, y_test), batch_size=sample_rate)
y_hat = model.predict(x_test)

# let's log the loss functions and plot them
train_loss = history.history['loss']
val_loss = history.history['val_loss']

plt.plot(train_loss, label='Training Loss')
plt.plot(val_loss, label='Validation Loss')
plt.axhline(0.5, color='black', ls='--')
plt.legend()
plt.title('Loss History')
plt.xlabel('Epochs')
plt.ylabel('Loss (MSE)')
plt.savefig('sine_regression_part1_loss.png')
plt.close()

y_hat = y_hat.reshape(y_hat.size)
clean = y_test - y_hat
mse = ((clean[-2*sample_rate:] - s[-2*sample_rate:])**2).mean()
plt.plot(t[30*sample_rate:], s[30*sample_rate:], label='True Signal')
plt.plot(t[30*sample_rate:], clean[-2*sample_rate:], label='True Signal Estimate', ls='--')
plt.plot(t[30*sample_rate:], (clean[-2*sample_rate:]-s[30*sample_rate:]), label='Residual')
plt.legend()
plt.title('MSE = {}'.format(mse))
plt.xlabel('Time [s]')
plt.ylabel('Amplitude')
plt.savefig('sine_regression_part1_mse.png')
plt.close()

noise_floor = 0.01 * np.random.rand(s.size)
freq, clean_psd = sig.welch(clean+noise_floor[:clean.size], fs=sample_rate, nperseg=sample_rate*8)
_, signal_psd = sig.welch(s+noise_floor, fs=sample_rate, nperseg=sample_rate*8)
_, d_psd = sig.welch(d+noise_floor, fs=sample_rate, nperseg=sample_rate*8)
plt.semilogy(freq, d_psd, label='System Signal')
plt.semilogy(freq, signal_psd, label='True Signal')
plt.semilogy(freq, clean_psd, label='True Signal Estimate')
plt.axvline(noise_freq, label='$f_n={}$ Hz'.format(noise_freq), ls='--', color='black')
plt.axvline(true_freq, label='$f_s={}$ Hz'.format(true_freq), ls='--', color='black')
plt.legend(loc='upper right')
plt.xlabel('Frequency [Hz]')
plt.ylabel('Power Spectral Density')
plt.xlim([3, 30])
plt.ylim([1e-9, 1e2])
plt.grid(True)
plt.title('PSD of Target and Network Output')
plt.savefig('sine_regression_part1_psd.png')
plt.close()


# start over without the phase shift and see how we do
d = s + n

x_train = n[:24*sample_rate]
y_train = d[:24*sample_rate]

x_test = n[24*sample_rate:]
y_test = d[24*sample_rate:]

model = Sequential()
model.add(Dense(2, input_dim=1, activation='linear'))
model.add(Dense(1, activation='linear'))
model.compile(loss='mse', optimizer='adam')
history = model.fit(x_train, y_train, epochs=60,
    validation_data=(x_test, y_test), batch_size=sample_rate, verbose=0)
y_hat = model.predict(x_test)
y_hat = y_hat.reshape(y_hat.size)

# let's log the loss functions and plot them
train_loss = history.history['loss']
val_loss = history.history['val_loss']
plt.plot(train_loss, label='Training Loss')
plt.plot(val_loss, label='Validation Loss')
plt.axhline(0.5, color='black', ls='--')
plt.legend()
plt.title('Loss History')
plt.xlabel('Epochs')
plt.ylabel('Loss (MSE)')
plt.savefig('sine_regression_part1_context_loss.png')
plt.close()

clean = y_test - y_hat
mse = ((clean[-2*sample_rate:] - s[-2*sample_rate:])**2).mean()
plt.plot(t[30*sample_rate:], s[30*sample_rate:], label='True Signal')
plt.plot(t[30*sample_rate:], clean[-2*sample_rate:], label='True Signal Estimate', ls='--')
plt.plot(t[30*sample_rate:], (clean[-2*sample_rate:]-s[30*sample_rate:]), label='Residual')
plt.legend()
plt.title('MSE = {}'.format(mse))
plt.xlabel('Time [s]')
plt.ylabel('Amplitude')
plt.savefig('sine_regression_part1_context_mse.png')
plt.close()

noise_floor = 0.01 * np.random.rand(s.size)
freq, clean_psd = sig.welch(clean+noise_floor[:clean.size], fs=sample_rate, nperseg=sample_rate*8)
_, signal_psd = sig.welch(s+noise_floor, fs=sample_rate, nperseg=sample_rate*8)
_, d_psd = sig.welch(d+noise_floor, fs=sample_rate, nperseg=sample_rate*8)
plt.semilogy(freq, d_psd, label='System Signal')
plt.semilogy(freq, signal_psd, label='True Signal')
plt.semilogy(freq, clean_psd, label='True Signal Estimate')
plt.axvline(noise_freq, label='$f_n={}$ Hz'.format(noise_freq), ls='--', color='black', alpha=0.5)
plt.axvline(true_freq, label='$f_s={}$ Hz'.format(true_freq), ls='--', color='black', alpha=0.5)
plt.legend(loc='upper right')
plt.xlabel('Frequency [Hz]')
plt.ylabel('Power Spectral Density')
plt.xlim([3, 30])
plt.ylim([1e-9, 1e2])
plt.grid(True)
plt.title('PSD of Target and Network Output')
plt.savefig('sine_regression_part1_context_psd.png')
plt.close()
