import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import numpy as np
np.random.seed(1337)
from keras.models import Sequential
from keras.layers import Dense

# make a list of 100 pairs from 0-25 for the network to add together
input_data = np.random.randint(25, size=(100,2))
target_data = np.array([x[0] + x[1] for x in input_data])
for ii in range(10):
    a, b = input_data[ii, :]
    ans = target_data[ii]
    print('{0:>2} + {1:>2} = {2:<10}'.format(a,b,ans))

train_size = 75

# this is the data we train the network on
x_train = input_data[:train_size, :]
y_train = target_data[:train_size]

# this is the data that we test to see how well we did during training
x_test = input_data[train_size:, :]
y_test = target_data[train_size:]

print('x_train.shape={0}, y_train.shape={1}'.format(x_train.shape, y_train.shape))
print('x_test.shape={0}, y_test.shape={1}'.format(x_test.shape, y_test.shape))

model = Sequential()
model.add(Dense(32, input_dim=x_train.shape[1], activation='tanh'))
model.add(Dense(16, activation='tanh'))
model.add(Dense(8, activation='tanh'))
model.add(Dense(4, activation='tanh'))
model.add(Dense(1, activation='linear'))
model.compile(loss='mse', optimizer='adam')
history = model.fit(x_train, y_train, epochs=300, validation_data=(x_test, y_test), batch_size=1)
y_hat = model.predict(x_test)

 # let's log the loss functions and plot them
train_loss = history.history['loss']
val_loss = history.history['val_loss']

plt.plot(train_loss, label='Training Loss')
plt.plot(val_loss, label='Validation Loss')
plt.legend()
plt.title('Loss History')
plt.xlabel('Epochs')
plt.ylabel('Loss (MSE)')
plt.savefig('overcomplicated_addition_loss.png')
plt.close()

# zoom in on the last 100 epochs
plt.plot(train_loss[-100:], label='Training Loss')
plt.plot(val_loss[-100:], label='Validation Loss')
plt.legend()
plt.title('Loss History of Last 100 Epochs')
plt.xlabel('Epochs')
plt.ylabel('Loss (MSE)')
plt.savefig('overcomplicated_addition_loss_last100.png')
plt.close()

for ii in range(x_test.shape[0]):
    a, b = x_test[ii, :]
    true_ans = y_test[ii]
    network_ans = y_hat[ii][0]
    print('{0:>2} + {1:>2} = {2:<10}'.format(a,b,network_ans))
