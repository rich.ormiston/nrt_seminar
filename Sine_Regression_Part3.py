# do imports
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import numpy as np
np.random.seed(1337)
import scipy.signal as sig
from keras.models import Sequential
from keras.layers import Dense, SimpleRNN, LSTM
import scipy.signal as sig


def do_lookback(data, steps=1, validation=False):
    temp = np.zeros((data.shape[0] - steps, steps + 1, data.shape[1]))
    temp[:, 0, :] = data[steps:, :]
    for i in range(temp.shape[0]):
        temp[i, 1:] = data[i:i + steps][::-1]

    if validation:
        temp = temp.reshape((temp.shape[0], temp.shape[1]))

    for i in range(temp.shape[0]):
        temp[i, :] = temp[i, :][::-1]

    return temp


# let's define the signals
dur = 128
sample_rate = 128
true_freq = 13.4
noise_freq = 5.8
noise_phase = np.pi/4
noise_amp = 0.25
t = np.linspace(0, dur, dur*sample_rate)
s = np.sin(2*np.pi*true_freq * t)
n = noise_amp * np.sin(2*np.pi*noise_freq*t + noise_phase)
d = s + noise_amp * np.sin(2*np.pi*noise_freq*t)  # phase shifted input

# let's see this phase shift
time = np.linspace(0, 1, sample_rate)
plt.plot(time, d[:sample_rate], label='system signal', lw=2.0)
plt.plot(time, noise_amp * np.sin(2*np.pi*noise_freq*t)[:sample_rate], label='added noise', ls='--')
plt.plot(time, n[:sample_rate], label='shifted noise', lw=2.0)
plt.title('System Data and Input Channel')
plt.xlabel('Time [s]')
plt.legend(loc='upper right')
plt.grid(True)
plt.ylim([-1.25, 1.25])
plt.savefig('sine_regression_part3_input.png')
plt.close()

# apply lookback
lb = 16  # NOTE: This is a lot
tar_lb = d[lb:]
wit_lb = do_lookback(n.reshape(n.size, 1), lb)

# reshape for training and testing (samples, lookback, features)
tt = 8  # test time in seconds
tfrac = 8*sample_rate
x_train = wit_lb[:-tfrac, :, :]
y_train = tar_lb[:-tfrac]

x_test = wit_lb[-tfrac:, :, :]
y_test = tar_lb[-tfrac:]

print('x_train shape:', x_train.shape, 'y_train shape:', y_train.shape)
print('x_test shape:', x_test.shape, 'y_test shape', y_test.shape)

# We can now run our first LSTM recurrent neural network!
model = Sequential()
model.add(LSTM(4, input_shape=(x_train.shape[1], x_train.shape[2]), activation='elu'))
model.add(Dense(2, activation='elu'))
model.add(Dense(1))
model.compile(loss='mse', optimizer='adam')
history = model.fit(x_train, y_train, epochs=40, validation_data=(x_test, y_test), batch_size=1000)

 # let's log the loss functions and plot them
train_loss = history.history['loss']
val_loss = history.history['val_loss']
plt.plot(train_loss, label='Training Loss')
plt.plot(val_loss, label='Validation Loss')
plt.axhline(0.5, ls='--', color='black')
plt.legend()
plt.title('Loss History')
plt.xlabel('Epochs')
plt.ylabel('Loss (MSE)')
plt.savefig('sine_regression_part3_loss.png')
plt.close()

# make predictions and plots
y_hat = model.predict(x_test)
y_hat = y_hat.flatten()
clean = y_test - y_hat

plt.plot(s[-sample_rate:], label='true signal')
plt.plot(clean[-sample_rate:], label='cleaned estimate', ls='--')
plt.title('LSTM Phase Correction')
plt.xlabel('Time [s]')
plt.legend(loc='upper right')
plt.grid(True)
plt.ylim([-1.25, 1.25])
plt.savefig('sine_regression_part3_estimate.png')
plt.close()

noise_floor = 0.01 * np.random.rand(sample_rate*8)
freq, clean_psd = sig.welch(clean+noise_floor, fs=sample_rate, nperseg=sample_rate*8)
_, signal_psd = sig.welch(s[-sample_rate*8:]+noise_floor, fs=sample_rate, nperseg=sample_rate*8)
_, d_psd = sig.welch(d[-sample_rate*8:]+noise_floor, fs=sample_rate, nperseg=sample_rate*8)
plt.semilogy(freq, d_psd, label='System Signal')
plt.semilogy(freq, signal_psd, label='True Signal')
plt.semilogy(freq, clean_psd, label='True Signal Estimate')
plt.axvline(noise_freq, label='$f_n={}$ Hz'.format(noise_freq), ls='--', color='black', alpha=0.5)
plt.axvline(true_freq, label='$f_s={}$ Hz'.format(true_freq), ls='--', color='black', alpha=0.5)
plt.legend(loc='upper right')
plt.xlabel('Frequency [Hz]')
plt.ylabel('Power Spectral Density')
plt.xlim([3,30])
plt.ylim([1e-9, 1e2])
plt.grid(True)
plt.title('PSD of Target and Network Output')
plt.savefig('sine_regression_part3_psd.png')
plt.close()
