import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('agg')
import numpy as np
import keras
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D, Flatten, Dense, Dropout

'''
We will have digits 0-9 (so ten in total). Each image is 28x28x1 pixels
The 1 in the input_shape is because this is black and white so 1 number fully
describes the pixel. In color, we have (R,G,B) values, so the 1 would be a 3
'''
num_classes = 10
input_shape = (28, 28, 1)

# collect the data (automatically split into training and testing sets)
(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()

# Scale images to the [0, 1] range
x_train = x_train.astype("float32") / 255.
x_test = x_test.astype("float32") / 255.

# Make sure images have shape (28, 28, 1)
x_train = np.expand_dims(x_train, -1)
x_test = np.expand_dims(x_test, -1)
print("x_train shape:", x_train.shape)
print(x_train.shape[0], "train samples")
print(x_test.shape[0], "test samples")

# plot some samples
for ii in range(9):
    plt.subplot(330+1+ii)
    plt.imshow(x_train[ii, :, :, 0], cmap='gray')
    frame = plt.gca()
    frame.axes.get_xaxis().set_visible(False)
    frame.axes.get_yaxis().set_visible(False)
plt.savefig('mnist_samples.png')
plt.close()

# do "one-hot" encoding on the target data
y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)

# build and run the network
model = Sequential()
model.add(Conv2D(32, kernel_size=(3, 3), activation="relu", input_shape=input_shape))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Conv2D(64, kernel_size=(3, 3), activation="relu"))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Flatten())
model.add(Dropout(0.5))
model.add(Dense(num_classes, activation="softmax"))

model.compile(loss="categorical_crossentropy", optimizer="adam", metrics=["accuracy"])
model.fit(x_train, y_train, batch_size=128, epochs=15, validation_split=0.1)

# Let's see how well we did
score = model.evaluate(x_test, y_test, verbose=0)
print("Test loss:", score[0])
print("Test accuracy:", score[1])
